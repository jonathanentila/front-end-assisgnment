// @author Jonathan Entila
// Front End Dev Test
// April 4 2018


function ChangeOptionsBar() {
    var optionX = document.getElementById("optionsbar").value;
   	//document.getElementById("progressWrap").innerHTML = "You selected: " + x;
   	//document.getElementById('progressWrap').classList.add('active');
   	if ( optionX == '1' ) {
   		document.getElementById('progressWrapOne').classList.add('active');
   		document.getElementById('progressWrapTwo').classList.remove('active');
   		document.getElementById('progressWrapThree').classList.remove('active');
   		document.getElementById('static').style.display = "none";

   		// to clean div
		var clearDiv = document.getElementById("buttons");
		clearDiv.innerHTML = '';

		var something = document.getElementById("buttons");
		var button = document.createElement("button");

		// Display the buttons upon select
		function progressAOne() {
		    // Get the width of the bar
			var width = document.getElementById('progressOneStatus').innerHTML;

			// Set the number of status and combine with strings
			var sum = 30 + +document.getElementById('progressOneStatus').innerHTML;
			if ( sum >= 210 ) {
				alert( 'not allow greater than 210' );
			} else {
				// view the result of the sum
				document.getElementById("progressOneStatus").innerHTML = sum;
				document.getElementById("progressOne").style.width = sum + '%';
				// if exceed the color of the bar should change to another color i make it red
				if ( sum >=100 ) {
					document.getElementById("progressOne").style.backgroundColor = "red";
				}
			}
		}

		button.id = "firstBarButton";
		button.textContent = "+31";
		button.addEventListener("click", progressAOne, false);

		something.appendChild(button);

		// Display the buttons upon select
		var something = document.getElementById("buttons");
		var button = document.createElement("button");

		// Display the buttons upon select
		function progressATwo() {
			var width = document.getElementById('progressOneStatus').innerHTML;
			
			// Set the number of status and combine with strings
			var sum = 48 + +document.getElementById('progressOneStatus').innerHTML;

			if ( sum >= 210 ) {
				alert( 'not allow greater than 210' );
			} else {

				// view the result of the sum
				document.getElementById("progressOneStatus").innerHTML = sum;
				document.getElementById("progressOne").style.width = sum + '%';
				// if exceed the color of the bar should change to another color i make it red
				if ( sum >=100 ) {
					document.getElementById("progressOne").style.backgroundColor = "red";
				}
			}
		}

		button.id = "firstBarButton";
		button.textContent = "+48";
		button.addEventListener("click", progressATwo, false);

		something.appendChild(button);

		// Display the buttons upon select
		var something = document.getElementById("buttons");
		var button = document.createElement("button");

		// Display the buttons upon select
		function progressNOne() {
			var width = document.getElementById('progressOneStatus').innerHTML;
			// Set the number of status and combine with strings
			var minus = +document.getElementById('progressOneStatus').innerHTML - 30;

			if ( minus <= 1 ) {
				alert( 'not allow lower than 0' );
			} else {
				// view the result of the minus
				document.getElementById("progressOneStatus").innerHTML = minus;
				document.getElementById("progressOne").style.width = +minus + '%';
				// if exceed the color of the bar should change to another color i make it red
				if ( minus >=100 ) {
					document.getElementById("progressOne").style.backgroundColor = "#333";
				}
			}
		}

		button.id = "firstBarButton";
		button.textContent = "-30";
		button.addEventListener("click", progressNOne, false);

		something.appendChild(button);


		// Display the buttons upon select
		var something = document.getElementById("buttons");
		var button = document.createElement("button");

		// Display the buttons upon select
		function progressNTwo() {
			var width = document.getElementById('progressOneStatus').innerHTML;
			//var id  = setInterval(frame, 60); // time frame function event

			// Set the number of status and combine with strings
			var minus = +document.getElementById('progressOneStatus').innerHTML - 6;
			if ( minus <= 1 ) {
				alert( 'not allow lower than 0' );
			} else {
				// view the result of the minus
				document.getElementById("progressOneStatus").innerHTML = minus;
				document.getElementById("progressOne").style.width = +minus + '%';
				// if exceed the color of the bar should change to another color i make it red
				if ( minus >=100 ) {
					document.getElementById("progressOne").style.backgroundColor = "#333";
				}
			}
		}

		button.id = "firstBarButton";
		button.textContent = "-6";
		button.addEventListener("click", progressNTwo, false);

		something.appendChild(button);

   	} else if ( optionX == '2' ) {
   		document.getElementById('progressWrapOne').classList.remove('active');
   		document.getElementById('progressWrapTwo').classList.add('active');
   		document.getElementById('progressWrapThree').classList.remove('active');
   		document.getElementById('static').style.display = "none";

   		// to clean div
		var clearDiv = document.getElementById("buttons");
		clearDiv.innerHTML = '';

		var something = document.getElementById("buttons");
		var button = document.createElement("button");

		// Display the buttons upon select
		function progressAOne() {
		    // Get the width of the bar
			var width = document.getElementById('progressTwoStatus').innerHTML;

			// Set the number of status and combine with strings
			var sum = 30 + +document.getElementById('progressTwoStatus').innerHTML;


			if ( sum >= 210 ) {
				alert( 'not allow greater than 210' );
			} else {
				// view the result of the sum
				document.getElementById("progressTwoStatus").innerHTML = sum;
				document.getElementById("progressTwo").style.width = sum + '%';
				// if exceed the color of the bar should change to another color i make it red
				if ( sum >=100 ) {
					document.getElementById("progressTwo").style.backgroundColor = "red";
				}
			}
		}

		button.id = "firstBarButton";
		button.textContent = "+31";
		button.addEventListener("click", progressAOne, false);

		something.appendChild(button);

		// Display the buttons upon select
		var something = document.getElementById("buttons");
		var button = document.createElement("button");

		// Display the buttons upon select
		function progressATwo() {
			var width = document.getElementById('progressTwoStatus').innerHTML;
			
			// Set the number of status and combine with strings
			var sum = 48 + +document.getElementById('progressTwoStatus').innerHTML;

			if ( sum >= 210 ) {
				alert( 'not allow greater than 210' );
			} else {			
				// view the result of the sum
				document.getElementById("progressTwoStatus").innerHTML = sum;
				document.getElementById("progressTwo").style.width = sum + '%';
				// if exceed the color of the bar should change to another color i make it red
				if ( sum >=100 ) {
					document.getElementById("progressTwo").style.backgroundColor = "red";
				}
			}
		}

		button.id = "firstBarButton";
		button.textContent = "+48";
		button.addEventListener("click", progressATwo, false);

		something.appendChild(button);

		// Display the buttons upon select
		var something = document.getElementById("buttons");
		var button = document.createElement("button");

		// Display the buttons upon select
		function progressNOne() {
			var width = document.getElementById('progressTwoStatus').innerHTML;
			//var id  = setInterval(frame, 60); // time frame function event

			// Set the number of status and combine with strings
			var minus = +document.getElementById('progressTwoStatus').innerHTML - 30;
			if ( minus <= 1 ) {
				alert( 'not allow lower than 0' );
			} else {

				// view the result of the minus
				document.getElementById("progressTwoStatus").innerHTML = minus;
				document.getElementById("progressTwo").style.width = +minus + '%';
				// if exceed the color of the bar should change to another color i make it red
				if ( minus >=100 ) {
					document.getElementById("progressTwo").style.backgroundColor = "#333";
				}
			}
		}

		button.id = "firstBarButton";
		button.textContent = "-30";
		button.addEventListener("click", progressNOne, false);

		something.appendChild(button);


		// Display the buttons upon select
		var something = document.getElementById("buttons");
		var button = document.createElement("button");

		// Display the buttons upon select
		function progressNTwo() {
			var width = document.getElementById('progressTwoStatus').innerHTML;
			//var id  = setInterval(frame, 60); // time frame function event

			// Set the number of status and combine with strings
			var minus = +document.getElementById('progressTwoStatus').innerHTML - 6;
			
			if ( minus <= 1 ) {
				alert( 'not allow lower than 0' );
			} else {
				// view the result of the minus
				document.getElementById("progressTwoStatus").innerHTML = minus;
				document.getElementById("progressTwo").style.width = +minus + '%';
				// if exceed the color of the bar should change to another color i make it red
				if ( minus >=100 ) {
					document.getElementById("progressTwo").style.backgroundColor = "#333";
				}
			}
		}

		button.id = "firstBarButton";
		button.textContent = "-6";
		button.addEventListener("click", progressNTwo, false);

		something.appendChild(button);

   	} else if ( optionX == '3' ) {
   		document.getElementById('progressWrapOne').classList.remove('active');
   		document.getElementById('progressWrapTwo').classList.remove('active');
   		document.getElementById('progressWrapThree').classList.add('active');
   		document.getElementById('static').style.display = "none";

   		// to clean div
		var clearDiv = document.getElementById("buttons");
		clearDiv.innerHTML = '';

		var something = document.getElementById("buttons");
		var button = document.createElement("button");

		// Display the buttons upon select
		function progressAOne() {
		    // Get the width of the bar
			var width = document.getElementById('progressThirdStatus').innerHTML;

			// Set the number of status and combine with strings
			var sum = 30 + +document.getElementById('progressThirdStatus').innerHTML;
			
			if ( sum >= 210 ) {
				alert( 'not allow greater than 210' );
			} else {
				// view the result of the sum
				document.getElementById("progressThirdStatus").innerHTML = sum;
				document.getElementById("progressThird").style.width = sum + '%';
				// if exceed the color of the bar should change to another color i make it red
				if ( sum >=100 ) {
					document.getElementById("progressThird").style.backgroundColor = "red";
				}
			}
		}

		button.id = "firstBarButton";
		button.textContent = "+31";
		button.addEventListener("click", progressAOne, false);

		something.appendChild(button);

		// Display the buttons upon select
		var something = document.getElementById("buttons");
		var button = document.createElement("button");

		// Display the buttons upon select
		function progressATwo() {
			var width = document.getElementById('progressThirdStatus').innerHTML;
			
			// Set the number of status and combine with strings
			var sum = 48 + +document.getElementById('progressThirdStatus').innerHTML;
			
			if ( sum >= 210 ) {
				alert( 'not allow greater than 210' );
			} else {
				// view the result of the sum
				document.getElementById("progressThirdStatus").innerHTML = sum;
				document.getElementById("progressThird").style.width = sum + '%';
				// if exceed the color of the bar should change to another color i make it red
				if ( sum >=100 ) {
					document.getElementById("progressThird").style.backgroundColor = "red";
				}
			}
		}

		button.id = "firstBarButton";
		button.textContent = "+48";
		button.addEventListener("click", progressATwo, false);

		something.appendChild(button);

		// Display the buttons upon select
		var something = document.getElementById("buttons");
		var button = document.createElement("button");

		// Display the buttons upon select
		function progressNOne() {
			var width = document.getElementById('progressThirdStatus').innerHTML;
			//var id  = setInterval(frame, 60); // time frame function event

			// Set the number of status and combine with strings
			var minus = +document.getElementById('progressThirdStatus').innerHTML - 30;
			// view the result of the minus
			
			if ( minus <= 1 ) {
				alert( 'not allow lower than 0' );
			} else {
				document.getElementById("progressThirdStatus").innerHTML = minus;
				document.getElementById("progressThird").style.width = +minus + '%';
				// if exceed the color of the bar should change to another color i make it red
				if ( minus >=100 ) {
					document.getElementById("progressThird").style.backgroundColor = "#333";
				}
			}
		}

		button.id = "firstBarButton";
		button.textContent = "-30";
		button.addEventListener("click", progressNOne, false);

		something.appendChild(button);


		// Display the buttons upon select
		var something = document.getElementById("buttons");
		var button = document.createElement("button");

		// Display the buttons upon select
		function progressNTwo() {
			var width = document.getElementById('progressThirdStatus').innerHTML;
			//var id  = setInterval(frame, 60); // time frame function event

			// Set the number of status and combine with strings
			var minus = +document.getElementById('progressThirdStatus').innerHTML - 6;
			if ( minus <= 1 ) {
				alert( 'not allow lower than 0' );
			} else {
				// view the result of the minus
				document.getElementById("progressThirdStatus").innerHTML = minus;
				document.getElementById("progressThird").style.width = +minus + '%';
				// if exceed the color of the bar should change to another color i make it red
				if ( minus >=100 ) {
					document.getElementById("progressThird").style.backgroundColor = "#333";
				}
			}
		}

		button.id = "firstBarButton";
		button.textContent = "-6";
		button.addEventListener("click", progressNTwo, false);

		something.appendChild(button);   		
  	}
}


// These functions is for static view only.
function progressAOne() {
	// Get the width of the bar
	var width = document.getElementById('progressOneStatus').innerHTML;

	// Set the number of status and combine with strings
	var sum = 30 + +document.getElementById('progressOneStatus').innerHTML;
			
	if ( sum >= 210 ) {
		alert( 'not allow greater than 210' );
	} else {
		// view the result of the sum
		document.getElementById("progressOneStatus").innerHTML = sum;
		document.getElementById("progressOne").style.width = sum + '%';
		// if exceed the color of the bar should change to another color i make it red
		if ( sum >=100 ) {
			document.getElementById("progressOne").style.backgroundColor = "red";
		}
	}
}

// Progress Button Tow 
function progressATwo() {
	var width = document.getElementById('progressOneStatus').innerHTML;
	
	// Set the number of status and combine with strings
	var sum = 48 + +document.getElementById('progressOneStatus').innerHTML;
	
	if ( sum >= 210 ) {
		alert( 'not allow greater than 210' );
	} else {
		// view the result of the sum
		document.getElementById("progressOneStatus").innerHTML = sum;
		document.getElementById("progressOne").style.width = sum + '%';
		// if exceed the color of the bar should change to another color i make it red
		if ( sum >=100 ) {
			document.getElementById("progressOne").style.backgroundColor = "red";
		}
	}
}


// Progress Button Three
function progressNOne() {
	var width = document.getElementById('progressOneStatus').innerHTML;
	//var id  = setInterval(frame, 60); // time frame function event

	// Set the number of status and combine with strings
	var minus = +document.getElementById('progressOneStatus').innerHTML - 30;
	if ( minus <= 1 ) {
		alert( 'not allow lower than 0' );
	} else {	

		// view the result of the minus
		document.getElementById("progressOneStatus").innerHTML = minus;
		document.getElementById("progressOne").style.width = +minus + '%';
		// if exceed the color of the bar should change to another color i make it red
		if ( minus >=100 ) {
			document.getElementById("progressOne").style.backgroundColor = "#333";
		}
	}

}

// Progress Button Four
function progressNTwo() {
	var width = document.getElementById('progressOneStatus').innerHTML;
	//var id  = setInterval(frame, 60); // time frame function event

	// Set the number of status and combine with strings
	var minus = +document.getElementById('progressOneStatus').innerHTML - 6;
	if ( minus <= 1 ) {
		alert( 'not allow lower than 0' );
	} else {	

		// view the result of the minus
		document.getElementById("progressOneStatus").innerHTML = minus;
		document.getElementById("progressOne").style.width = +minus + '%';
		// if exceed the color of the bar should change to another color i make it red
		if ( minus >=100 ) {
			document.getElementById("progressOne").style.backgroundColor = "#333";
		}
	}

}